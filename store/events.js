import axios from 'axios'
export const state = () => ({
  events: []
})

export const getters = {
  events: (state) => {
    return state.events
  }
}

export const mutations = {
  setEvents (state, events) {
    state.events = events
  }
}

export const actions = {
  async getEvents (context) {
    return new Promise((resolve, reject) => {
      axios.get('/rest/events').then((response) => {
        context.commit('setEvents', response.data)
        resolve()
      }).catch((response) => {
        reject(response)
      })
    })
  }
}
