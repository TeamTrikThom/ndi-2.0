export const state = () => ({
  snackbar: {
    value: false,
    text: '',
    icon: 'check_circle',
    color: 'primary'
  }
})

export const mutations = {
  setSnackbar (state, snackbar) {
    state.snackbar = snackbar
  },
  setValue (state, value) {
    state.snackbar.value = value
  }
}

export const getters = {
  snackbar: (state) => {
    return state.snackbar
  }
}
