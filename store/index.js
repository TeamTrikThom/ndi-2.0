import axios from 'axios'

export const state = () => ({
  user: null
})
export const mutations = {
  setUser (state, user) {
    state.user = user
  }
}
export const actions = {
  login (context, user) {
    return new Promise((resolve, reject) => {
      axios.post('/api/login', user).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error.message)
      })
    })
  },
  logout (context) {
    return new Promise((resolve, reject) => {
      axios.post('/api/logout').then(() => {
        context.commit('setUser', null)
        resolve()
      }).catch((error) => {
        reject(error.message)
      })
    })
  }
}
