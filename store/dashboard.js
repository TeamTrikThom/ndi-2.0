import axios from 'axios'
export const state = () => ({
  users: []
})

export const getters = {
  users: (state) => {
    return state.users
  }
}

export const mutations = {
  setUsers (state, users) {
    state.users = users
  }
}

export const actions = {
  async getUsers (context) {
    return new Promise((resolve, reject) => {
      axios.get('/rest/users').then((response) => {
        context.commit('setUsers', response.data)
        resolve()
      }).catch((response) => {
        reject(response)
      })
    })
  }
}
