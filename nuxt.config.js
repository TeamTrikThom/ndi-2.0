
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'NDI',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'NDI' },
      { lang: 'nl' }
    ],

    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' },
      { rel: 'manifest', href: '/manifest.json' }
    ]
  },
  plugins: [
    { src: '~/plugins/vuetify.js', ssr: true }
  ],
  router: {
    middleware: ['authentication']
  },
  css: [
    '~/assets/style/app.styl'
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3ECE77' },
  modules: [
    '@nuxtjs/proxy'
  ],
  proxy: {
    '/api/login': {target: 'http://127.0.0.1:8080/rest', ws: false},
    '/api/logout': {target: 'http://127.0.0.1:8080/rest', ws: false},
    '/rest': {target: 'http://127.0.0.1:8080', ws: false}
  },
  /*
  ** Build configuration
  */
  build: {
    vendor: [
      'vuetify'
    ],
    extractCSS: true,
    cssSourceMap: false,
    /*
    ** Run ESLint on save
    */
    /*
    extend (config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
    */
  }
}
