export default function ({ store, route, redirect }) {
  const isLoggedIn = store.state.user
  if (route.path === '/dashboard' && !isLoggedIn) {
    redirect('/')
  }
}
